// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

module.export = require('@babel/register')({
  extensions: ['.ts', '.tsx'],
  only: ['./'],
  plugins: [
    [
      'babel-plugin-transform-import-ignore',
      {
        patterns: ['.less', '*custom-antd-components*'],
      },
    ],
  ],
  presets: ['@babel/typescript', '@babel/react'],
});
