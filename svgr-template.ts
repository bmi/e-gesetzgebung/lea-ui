// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const template = (variables, { tpl }) => {
  return tpl`
${variables.imports};

${variables.interfaces};

const ${`${variables.componentName}: React.FunctionComponent<React.SVGProps<SVGSVGElement>>`} = (${
    variables.props
  }) => (
  ${variables.jsx}
);
 
${variables.exports};
`;
};

module.exports = template;
