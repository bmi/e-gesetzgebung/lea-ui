// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

module.exports = {
  multipass: true, // boolean. false by default
  //datauri: 'enc', // 'base64', 'enc' or 'unenc'. 'base64' by default
  js2svg: {
    indent: 2, // string with spaces or number of spaces. 4 by default
    pretty: true, // boolean, false by default
  },
  plugins: [
    {
      name: 'cleanupAttrs',
      active: true,
    },
    {
      name: 'cleanupEnableBackground',
      active: true,
    },
    {
      name: 'cleanupIDs',
      active: true,
    },
    {
      name: 'cleanupNumericValues',
      active: true,
    },
    {
      name: 'collapseGroups',
      active: true,
    },
    {
      name: 'convertColors',
      active: true,
    },
    {
      name: 'convertPathData',
      active: true,
      params: {
        noSpaceAfterFlags: false,
      },
    },
    {
      name: 'convertShapeToPath',
      active: true,
    },
    {
      name: 'convertStyleToAttrs',
      active: false,
    },
    {
      name: 'convertTransform',
      active: true,
    },
    {
      name: 'inlineStyles',
      active: true,
    },
    {
      name: 'mergePaths',
      active: true,
      params: {
        noSpaceAfterFlags: false,
      },
    },
    {
      name: 'minifyStyles',
      active: false,
    },
    {
      name: 'moveElemsAttrsToGroup',
      active: true,
    },
    {
      name: 'moveGroupAttrsToElems',
      active: true,
    },
    {
      name: 'removeAttrs',
      active: true,
      params: {
        attrs: '(data-name|id)',
      },
    },
    {
      name: 'removeComments',
      active: true,
    },
    {
      name: 'removeDesc',
      active: true,
    },
    {
      name: 'removeDimensions',
      active: true,
    },
    {
      name: 'removeDoctype',
      active: false,
    },
    {
      name: 'removeEditorsNSData',
      active: true,
    },
    {
      name: 'removeEmptyAttrs',
      active: true,
    },
    {
      name: 'removeEmptyContainers',
      active: true,
    },
    {
      name: 'removeEmptyText',
      active: true,
    },
    {
      name: 'removeHiddenElems',
      active: true,
    },
    {
      name: 'removeMetadata', //keep rdf-data
      active: false,
    },
    {
      name: 'removeNonInheritableGroupAttrs',
      active: true,
    },
    {
      name: 'removeTitle',
      active: true,
    },
    {
      name: 'removeUnknownsAndDefaults',
      active: true,
      params: {
        keepRoleAttr: true,
      },
    },
    {
      name: 'removeUnusedNS',
      active: true,
    },
    {
      name: 'removeUselessDefs',
      active: true,
    },
    {
      name: 'removeUselessStrokeAndFill',
      active: true,
    },
    {
      name: 'removeViewBox',
      active: true,
    },
    {
      name: 'removeXMLNS',
      active: false,
    },
    {
      name: 'removeXMLProcInst',
      active: false,
    },
    {
      name: 'sortAttrs',
      active: true,
    },
  ],
};
