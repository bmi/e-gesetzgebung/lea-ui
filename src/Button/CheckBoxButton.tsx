// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';

import './CheckBoxButton.less';

interface CheckBoxButtonProps {
  id: string;
  ariaDescribedby: string;
  label: string;
  checked: boolean;
  handleOnChange: () => void;
  refProp?: React.RefObject<HTMLInputElement>;
}

export const CheckBoxButton: FunctionComponent<CheckBoxButtonProps> = ({
  id,
  ariaDescribedby,
  label,
  checked,
  handleOnChange,
  refProp,
}) => {
  return (
    <div className="lea-ui-checkbox-button">
      <div className="input-wrapper">
        <input
          id={id}
          ref={refProp}
          aria-describedby={ariaDescribedby}
          name={id}
          type="checkbox"
          checked={checked}
          onChange={() => handleOnChange()}
        />
      </div>
      <div className="content">
        <label htmlFor={id}>{label}</label>
      </div>
    </div>
  );
};
