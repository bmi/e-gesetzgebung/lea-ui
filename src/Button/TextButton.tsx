// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';

type IconButtonProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
  buttonRef?: React.RefObject<HTMLButtonElement>;
  onHover: (hovered: boolean) => void;
};

const TextButton: FunctionComponent<IconButtonProps> = ({ children, buttonRef, onHover, ...props }) => {
  return (
    <button
      className="lea-ui-text-button"
      type="button"
      ref={buttonRef}
      onMouseOver={() => onHover && onHover(true)}
      onMouseOut={() => onHover && onHover(false)}
      onFocus={() => onHover && onHover(true)}
      onBlur={() => onHover && onHover(false)}
      {...props}
    >
      {children}
    </button>
  );
};

export default TextButton;
