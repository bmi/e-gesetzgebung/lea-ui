// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';

import './IconButton.less';
type IconButtonProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
  icon: React.JSX.Element;
  iconPosition?: 'left' | 'right';
  onHover?: (hovered: boolean) => void;
  buttonRef?: React.RefObject<HTMLButtonElement>;
};

const IconButton: FunctionComponent<IconButtonProps> = ({
  icon,
  iconPosition,
  children,
  onHover,
  buttonRef,
  ...props
}) => {
  iconPosition = iconPosition ?? 'left';
  return (
    <button
      className="lea-ui-icon-button"
      type="button"
      ref={buttonRef}
      onMouseEnter={() => onHover && onHover(true)}
      onMouseLeave={() => onHover && onHover(false)}
      onFocus={() => onHover && onHover(true)}
      onBlur={() => onHover && onHover(false)}
      {...props}
    >
      {iconPosition === 'left' && icon}
      {children}
      {iconPosition === 'right' && icon}
    </button>
  );
};

export default IconButton;
