// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconCreateAuthorialNoteDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <g id="Gruppe_12321" data-name="Gruppe 12321" transform="translate(-896 -170)">
      <g id="Gruppe_5772" data-name="Gruppe 5772" transform="translate(-110 58)">
        <rect
          id="Rechteck_1630"
          data-name="Rechteck 1630"
          width="24"
          height="24"
          transform="translate(1006 112)"
          fill="none"
        />
      </g>
      <path
        id="Vereinigungsmenge_1"
        data-name="Vereinigungsmenge 1"
        d="M5543.99,13555.374v.82a.175.175,0,0,1-.177.177h-1.3a.175.175,0,0,1-.173-.177v-12.021a.174.174,0,0,1,.173-.173h1.3a.175.175,0,0,1,.177.173v4.423a2.637,2.637,0,0,1,2.341-1.153,3.206,3.206,0,0,1,2.63,1.256,6.173,6.173,0,0,1,0,6.61,3.206,3.206,0,0,1-2.63,1.256A2.634,2.634,0,0,1,5543.99,13555.374Zm0-4.978v3.117c0,.762.613,1.53,1.984,1.53a2.12,2.12,0,0,0,2.2-2.327v-1.429a2.119,2.119,0,0,0-2.2-2.323C5544.861,13548.965,5543.99,13549.594,5543.99,13550.4Zm-10.989,3.46a2.262,2.262,0,0,1,.946-1.963,4.879,4.879,0,0,1,2.8-.667h1.48v-.652c0-1.116-.592-1.661-1.807-1.661a2.163,2.163,0,0,0-1.977,1.167.177.177,0,0,1-.126.082.169.169,0,0,1-.146-.044l-.779-.731a.176.176,0,0,1-.034-.208,3.377,3.377,0,0,1,3.126-1.735,3.71,3.71,0,0,1,2.48.793,2.841,2.841,0,0,1,.912,2.208v4.44h.783a.172.172,0,0,1,.177.173v1.137a.175.175,0,0,1-.177.177h-.731a1.459,1.459,0,0,1-1.531-1.1,2.6,2.6,0,0,1-2.416,1.29C5534.145,13556.565,5533,13555.527,5533,13553.856Zm1.715-.211v.341c0,.7.551,1.123,1.48,1.123,1.16,0,2.035-.579,2.035-1.352v-1.2h-1.548C5534.716,13552.554,5534.716,13553.374,5534.716,13553.646Zm16.674-2.351a.175.175,0,0,1-.177-.177v-.7a.175.175,0,0,1,.177-.173h1.671v-4.812l-1.439,1.341a.16.16,0,0,1-.126.048.193.193,0,0,1-.122-.055l-.463-.5a.172.172,0,0,1,.01-.244l1.752-1.641a.172.172,0,0,1,.119-.047h1.242a.174.174,0,0,1,.174.173v5.74h1.531a.172.172,0,0,1,.174.173v.7a.173.173,0,0,1-.174.177Z"
        transform="translate(-4636.476 -13368.267)"
      />
    </g>
  </svg>
);

export default IconCreateAuthorialNoteDefault;
