// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconOutlinedNumberingSymbolsDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" {...props}>
    <path
      d="M0 0h24v24H0z"
      style={{
        fill: '#fff',
      }}
    />
    <g transform="translate(12.268 -23.346)">
      <path d="M7.542 31.1h-9a.75.75 0 0 1-.75-.75.75.75 0 0 1 .75-.75h9a.75.75 0 0 1 0 1.5Zm0 5h-9a.75.75 0 0 1-.75-.75.75.75 0 0 1 .75-.75h9a.75.75 0 0 1 0 1.5Zm0 5h-9a.75.75 0 0 1-.75-.75.75.75 0 0 1 .75-.75h9a.75.75 0 0 1 0 1.5Z" />
      <circle cx={1.25} cy={1.25} r={1.25} transform="translate(-7.268 29.096)" />
      <circle cx={1.25} cy={1.25} r={1.25} transform="translate(-7.268 34.096)" />
      <circle cx={1.25} cy={1.25} r={1.25} transform="translate(-7.268 39.096)" />
    </g>
  </svg>
);

export default SvgIconOutlinedNumberingSymbolsDefault;
