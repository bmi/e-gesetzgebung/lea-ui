// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconCreateCommentActive: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg id="icon-comments-active" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <g id="Gruppe_5772" data-name="Gruppe 5772" transform="translate(-1006 -112)">
      <rect
        id="Rechteck_1630"
        data-name="Rechteck 1630"
        width="24"
        height="24"
        transform="translate(1006 112)"
        fill="#0032d9"
      />
    </g>
    <g id="Gruppe_12324" data-name="Gruppe 12324" transform="translate(-241.723 -227.053)">
      <path
        id="Rechteck_2718"
        data-name="Rechteck 2718"
        d="M244.689,231.3h18a.75.75,0,0,1,.75.75v13a.75.75,0,0,1-.75.75H252.654l-4.526,3.537a.75.75,0,0,1-1.212-.581l-.038-2.956h-2.19a.75.75,0,0,1-.75-.75v-13A.75.75,0,0,1,244.689,231.3Zm17.25,1.5h-16.5v11.5h2.18a.75.75,0,0,1,.75.74l.028,2.183,3.537-2.764a.75.75,0,0,1,.462-.159h9.543Z"
        fill="#fff"
      />
      <circle
        id="Ellipse_568"
        data-name="Ellipse 568"
        cx="1.338"
        cy="1.338"
        r="1.338"
        transform="translate(248.235 237.138)"
        fill="#fff"
      />
      <circle
        id="Ellipse_569"
        data-name="Ellipse 569"
        cx="1.338"
        cy="1.338"
        r="1.338"
        transform="translate(252.351 237.138)"
        fill="#fff"
      />
      <circle
        id="Ellipse_570"
        data-name="Ellipse 570"
        cx="1.338"
        cy="1.338"
        r="1.338"
        transform="translate(256.467 237.138)"
        fill="#fff"
      />
    </g>
  </svg>
);

export default IconCreateCommentActive;
