// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';

const IconPin: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = () => (
  <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M16.791 13.0376H16.7687L16.1194 4.26251C16.6791 4.26251 17.1269 3.80902 17.1269 3.24216V2.44855C17.1269 2.17645 16.903 1.94971 16.6343 1.94971H7.34328C7.07463 1.94971 6.85075 2.17645 6.85075 2.44855V3.24216C6.85075 3.80902 7.29851 4.26251 7.85821 4.26251L7.20896 13.0376H7.18657C6.53731 13.0376 6 13.5818 6 14.2393V15.1463C6 15.4637 6.24627 15.7358 6.58209 15.7358H11V20.2481L11.8657 23.8307C11.8881 23.9894 12.1119 23.9894 12.1343 23.8307L13 20.2481V15.7358H17.4179C17.7313 15.7358 18 15.4864 18 15.1463V14.2393C18 13.5818 17.4627 13.0376 16.8134 13.0376H16.791Z"
      fill="#01012B"
    />
  </svg>
);

export default IconPin;
