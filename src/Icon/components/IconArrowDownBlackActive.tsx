// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconArrowDownBlackActive: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg
    id="arrow_down_black_active"
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    {...props}
  >
    <rect id="Rechteck_1527" data-name="Rechteck 1527" width="24" height="24" fill="#0032d9" />
    <path
      id="Polygon_2"
      data-name="Polygon 2"
      d="M4,0,8,5H0Z"
      transform="translate(15.852 15) rotate(180)"
      fill="#fff"
    />
  </svg>
);

export default SvgIconArrowDownBlackActive;
