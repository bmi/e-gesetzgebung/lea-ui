// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconOutlinedNumberingSymbolsHover: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg
    id="icon-outlined-numbering-symbols-active"
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    {...props}
  >
    <g id="Gruppe_2875" data-name="Gruppe 2875" transform="translate(-1006 -112)">
      <rect
        id="Rechteck_1630"
        data-name="Rechteck 1630"
        width="24"
        height="24"
        transform="translate(1006 112)"
        fill="#0032d9"
      />
    </g>
    <g id="Gruppe_6715" data-name="Gruppe 6715" transform="translate(-1205 -120)">
      <line
        id="Linie_1038"
        data-name="Linie 1038"
        x2="9"
        transform="translate(1215.809 127)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <line
        id="Linie_1039"
        data-name="Linie 1039"
        x2="9"
        transform="translate(1215.809 132)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <line
        id="Linie_1040"
        data-name="Linie 1040"
        x2="9"
        transform="translate(1215.809 137)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <circle
        id="Ellipse_557"
        data-name="Ellipse 557"
        cx="1.25"
        cy="1.25"
        r="1.25"
        transform="translate(1210 125.75)"
        fill="#fff"
      />
      <circle
        id="Ellipse_558"
        data-name="Ellipse 558"
        cx="1.25"
        cy="1.25"
        r="1.25"
        transform="translate(1210 130.75)"
        fill="#fff"
      />
      <circle
        id="Ellipse_559"
        data-name="Ellipse 559"
        cx="1.25"
        cy="1.25"
        r="1.25"
        transform="translate(1210 135.75)"
        fill="#fff"
      />
    </g>
  </svg>
);

export default SvgIconOutlinedNumberingSymbolsHover;
