// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconUndoDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (props: SVGProps<SVGSVGElement>) => (
  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <svg width="15.932" height="15.789" x="8" y="8" viewBox="0 0 15.932 15.789">
      <g fill="none" stroke="#161615" strokeLinecap="round" strokeWidth="1.5px" transform="translate(-4.11 -3.858)">
        <path
          strokeMiterlimit="10"
          d="M122.524,75.327h9.061a5.119,5.119,0,1,1,0,10.227h-3.056"
          transform="translate(-117.664 -66.656)"
        />
        <path
          strokeLinejoin="round"
          d="M126.274,81.094l-3.751-3.751,3.751-3.751"
          transform="translate(-117.664 -68.674)"
        />
      </g>
    </svg>
  </svg>
);

export default IconUndoDefault;
