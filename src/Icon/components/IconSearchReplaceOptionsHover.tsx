// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconSearchReplaceOptionsHover: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="32"
    height="32"
    viewBox="0 0 32 32"
    aria-describedby="search-replace-title"
    role="img"
  >
    <title id="search-replace-title">Suchen / Ersetzen Schalter</title>
    <svg>
      <g id="Rechteck_4747" data-name="Rechteck 4747" stroke="#000" strokeWidth="1">
        <rect width="32" height="32" rx="3" stroke="none" />
        <rect x="0.5" y="0.5" width="31" height="31" rx="2.5" fill="none" />
      </g>
    </svg>
    <svg x="7.1" y="7.47">
      <g id="Gruppe_11557" data-name="Gruppe 11557" transform="translate(-147.41 -323.95)">
        <path
          id="Pfad_7801"
          data-name="Pfad 7801"
          d="M163.24,338.56l.01-.01-4.03-4.03A6.568,6.568,0,0,0,154,323.95h-.01a6.58,6.58,0,0,0-.01,13.16h.01a6.6,6.6,0,0,0,3.99-1.35l3.95,3.94a.882.882,0,0,0,.6.24.894.894,0,0,0,.63-.26A.866.866,0,0,0,163.24,338.56ZM154,325.7a4.853,4.853,0,0,1,4.83,4.83,4.831,4.831,0,0,1-4.83,4.82h-.01a4.838,4.838,0,0,1-4.82-4.7v-.12A4.84,4.84,0,0,1,154,325.7Z"
          fill="#fff"
        />
      </g>
    </svg>
  </svg>
);

export default IconSearchReplaceOptionsHover;
