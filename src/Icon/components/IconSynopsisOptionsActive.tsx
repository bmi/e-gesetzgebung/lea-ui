// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconSynopsisOptionsActive: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
    <svg>
      <g id="Rechteck_4376" data-name="Rechteck 4376" fill="#0032d9" stroke="#0032d9" strokeWidth="1">
        <rect width="32" height="32" rx="3" stroke="none" />
        <rect x="0.5" y="0.5" width="31" height="31" rx="2.5" fill="none" />
      </g>
    </svg>
    <svg x="6.2" y="7.47">
      <g id="Icon_parallelansicht-hover" transform="translate(-0.2)" clipPath="url(#clip-path)">
        <path
          id="Pfad_19657"
          data-name="Pfad 19657"
          d="M21.3,13.46a3.251,3.251,0,0,0,.013-1.038L21.3,12.4V12.36l.623-.537a.3.3,0,0,0,.11-.232.185.185,0,0,0-.024-.11V11.46a3.981,3.981,0,0,0-.77-1.319l-.012-.024a.384.384,0,0,0-.146-.1.365.365,0,0,0-.2.012l-.769.269a3.7,3.7,0,0,0-.952-.549l-.158-.806V8.931a.286.286,0,0,0-.245-.232l-.024-.012a4.545,4.545,0,0,0-1.526,0l-.024.012a.312.312,0,0,0-.244.244l-.159.818a3.165,3.165,0,0,0-.94.549l-.794-.281a.289.289,0,0,0-.329.086l-.025.024a4.311,4.311,0,0,0-.756,1.319l-.013.024a.216.216,0,0,0-.012.086.365.365,0,0,0,.1.244l.012.012.635.537a3.323,3.323,0,0,0,0,1.1L14.028,14a.33.33,0,0,0-.1.342l.013.024a4.177,4.177,0,0,0,.769,1.318l.012.025a.307.307,0,0,0,.244.11c.024,0,.061-.012.1-.012l.781-.281a3.447,3.447,0,0,0,.94.549l.159.806a.284.284,0,0,0,.244.244l.024.012a4.035,4.035,0,0,0,.757.061h.012a3.958,3.958,0,0,0,.757-.073h.024a.316.316,0,0,0,.245-.244l.158-.806a3.4,3.4,0,0,0,.952-.562l.769.281a.3.3,0,0,0,.342-.085l.012-.025a4.1,4.1,0,0,0,.77-1.318V14.34a.292.292,0,0,0,.024-.135.314.314,0,0,0-.11-.207ZM17.985,14.6a1.674,1.674,0,1,1,.646-.126,1.684,1.684,0,0,1-.646.126Z"
          fill="#fff"
        />
        <path
          id="Pfad_19658"
          data-name="Pfad 19658"
          d="M18.95,0H.95A.749.749,0,0,0,.2.75v14a.755.755,0,0,0,.75.75H11.43V14H7.63V1.5H18.294V6.06H19.7V.75A.749.749,0,0,0,18.95,0ZM6.13,14H1.7V1.5H6.13Z"
          fill="#fff"
        />
      </g>
    </svg>
  </svg>
);

export default IconSynopsisOptionsActive;
