// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';
import { useTranslation } from 'react-i18next';

const SvgIconFolderCompoundDocumentDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => {
  const { t } = useTranslation();
  return (
    <svg
      width="16"
      height="12"
      viewBox="0 0 16 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      aria-labelledby="title"
      {...props}
    >
      <title>{t('leaTheme.icon.compoundDocumentIcon')}</title>
      <path
        d="M15.499 2.83003V11C15.499 11.1327 15.4463 11.2598 15.3525 11.3536C15.2588 11.4474 15.1316 11.5 14.999 11.5H0.999023C0.867058 11.498 0.741091 11.4446 0.647766 11.3513C0.554441 11.258 0.501086 11.132 0.499023 11V1.00004C0.499023 0.867436 0.551679 0.740236 0.645447 0.646468C0.739215 0.5527 0.866415 0.500045 0.999023 0.500045H5.85901C5.9215 0.497848 5.98377 0.508201 6.04218 0.530532C6.10058 0.552863 6.15395 0.586722 6.19904 0.630049L8.04901 2.33003H14.999C15.1316 2.33003 15.2588 2.38272 15.3525 2.47649C15.4463 2.57025 15.499 2.69742 15.499 2.83003Z"
        fill="#9DAFEC"
      />
    </svg>
  );
};

export default SvgIconFolderCompoundDocumentDefault;
