// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconEyeDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (props: SVGProps<SVGSVGElement>) => (
  <svg id="icon-eye-default" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <rect id="Rechteck_1924" data-name="Rechteck 1924" width="24" height="24" fill="none" />
    <g id="J7nHQU" transform="translate(251.365 -34.992)">
      <g id="Gruppe_11712" data-name="Gruppe 11712" transform="translate(-245.141 42.949)">
        <path
          id="Pfad_8655"
          data-name="Pfad 8655"
          d="M-245.141,46.981c.054-.105.105-.212.162-.316a7.967,7.967,0,0,1,1.786-2.252,5.989,5.989,0,0,1,1.947-1.148A5.521,5.521,0,0,1-240,42.984a5.675,5.675,0,0,1,2.424.25,6.019,6.019,0,0,1,2.231,1.352,7.974,7.974,0,0,1,1.721,2.331c.011.023.022.046.037.08-.085.16-.168.328-.261.49a7.926,7.926,0,0,1-1.683,2.074,5.887,5.887,0,0,1-2.441,1.3,5.676,5.676,0,0,1-1.774.158,5.685,5.685,0,0,1-3.176-1.233,7.688,7.688,0,0,1-1.722-1.909,7.11,7.11,0,0,1-.483-.863.1.1,0,0,0-.011-.015Zm3.176.008a2.6,2.6,0,0,0,2.6,2.6,2.6,2.6,0,0,0,2.6-2.594,2.6,2.6,0,0,0-2.6-2.6A2.6,2.6,0,0,0-241.965,46.989Z"
          transform="translate(245.141 -42.949)"
          fill="#fff"
        />
        <path
          id="Pfad_8656"
          data-name="Pfad 8656"
          d="M207.38,314.172a1.443,1.443,0,1,1-1.446-1.442A1.444,1.444,0,0,1,207.38,314.172Z"
          transform="translate(-200.162 -310.131)"
          fill="#fff"
        />
      </g>
    </g>
  </svg>
);

export default IconEyeDefault;
