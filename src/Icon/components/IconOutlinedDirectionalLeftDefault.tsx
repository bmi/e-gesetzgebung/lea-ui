// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconOutlinedDirectionalLeftDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M5.9991 11.741C6.09764 11.7415 6.19528 11.7222 6.28629 11.6845C6.3773 11.6467 6.45985 11.5911 6.5291 11.521C6.66955 11.3804 6.74844 11.1897 6.74844 10.991C6.74844 10.7922 6.66955 10.6016 6.5291 10.461L2.0591 5.991L6.5291 1.521C6.66158 1.37882 6.7337 1.19078 6.73027 0.996474C6.72684 0.802173 6.64813 0.616788 6.51072 0.479375C6.3733 0.341962 6.18792 0.26325 5.99362 0.259821C5.79932 0.256393 5.61127 0.328516 5.4691 0.460996L0.469096 5.461C0.328645 5.60162 0.249756 5.79225 0.249756 5.991C0.249756 6.18975 0.328645 6.38037 0.469096 6.521L5.4691 11.521C5.53834 11.5911 5.62089 11.6467 5.7119 11.6845C5.80291 11.7222 5.90056 11.7415 5.9991 11.741Z"
      fill="black"
    />
  </svg>
);

export default SvgIconOutlinedDirectionalLeftDefault;
