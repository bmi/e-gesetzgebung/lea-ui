// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconCommentOptionsHover: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="32"
    height="32"
    viewBox="0 0 32 32"
    aria-describedby="comment-options-title"
    role="img"
  >
    <title id="comment-options-title">Kommentaroptionen Schalter</title>
    <svg>
      <g id="Rechteck_4376" data-name="Rechteck 4376" stroke="#000" strokeWidth="1">
        <rect width="32" height="32" rx="3" stroke="none" />
        <rect x="0.5" y="0.5" width="31" height="31" rx="2.5" fill="none" />
      </g>
    </svg>
    <svg x="5.2" y="6.47">
      <g id="Gruppe_11553" data-name="Gruppe 11553" transform="translate(7498.798 -3041.468)">
        <g id="Gruppe_11346" data-name="Gruppe 11346" transform="translate(-7659.918 2753.886)">
          <g id="Gruppe_11331" data-name="Gruppe 11331">
            <path
              id="Pfad_7769"
              data-name="Pfad 7769"
              d="M166.167,297.42a2.089,2.089,0,1,0-2.089-2.088A2.089,2.089,0,0,0,166.167,297.42Zm0-2.677a.589.589,0,1,1-.589.589.59.59,0,0,1,.589-.589Z"
              fill="#fff"
            />
            <path
              id="Pfad_7770"
              data-name="Pfad 7770"
              d="M171.461,297.42a2.089,2.089,0,1,0-2.089-2.088A2.089,2.089,0,0,0,171.461,297.42Zm0-2.677a.589.589,0,1,1-.589.589.589.589,0,0,1,.589-.589Z"
              fill="#fff"
            />
            <path
              id="Pfad_7772"
              data-name="Pfad 7772"
              d="M180.627,294.2m0,0v-5.871a.749.749,0,0,0-.748-.751H161.87a.75.75,0,0,0-.75.75v14a.75.75,0,0,0,.75.75h2.208V305.9a.75.75,0,0,0,1.208.594l4.426-3.408h2.336v-1.5h-2.594a.745.745,0,0,0-.458.157l-3.418,2.633V302.33a.75.75,0,0,0-.75-.75H162.62v-12.5h16.505V294.2Z"
              fill="#fff"
            />
          </g>
          <g id="Gruppe_6444" data-name="Gruppe 6444">
            <path
              id="Pfad_7791"
              data-name="Pfad 7791"
              d="M182.227,301.7a3.282,3.282,0,0,0,.012-1.038l-.012-.024V300.6l.623-.537a.3.3,0,0,0,.11-.232.178.178,0,0,0-.025-.11V299.7a3.979,3.979,0,0,0-.769-1.319l-.012-.024a.4.4,0,0,0-.147-.1.361.361,0,0,0-.2.012l-.769.269a3.7,3.7,0,0,0-.952-.549l-.159-.806v-.012a.284.284,0,0,0-.244-.232l-.024-.012a4.545,4.545,0,0,0-1.526,0l-.025.012a.312.312,0,0,0-.244.244l-.159.818a3.177,3.177,0,0,0-.94.549l-.793-.281a.291.291,0,0,0-.33.086l-.024.024a4.312,4.312,0,0,0-.757,1.319l-.012.024a.214.214,0,0,0-.012.086.363.363,0,0,0,.1.244l.013.012.634.537a3.323,3.323,0,0,0,0,1.1l-.634.537a.328.328,0,0,0-.1.342l.012.024a4.211,4.211,0,0,0,.769,1.318l.012.025a.309.309,0,0,0,.245.11c.024,0,.061-.012.1-.012l.781-.281a3.46,3.46,0,0,0,.94.549l.159.806a.284.284,0,0,0,.244.244l.025.012a4.012,4.012,0,0,0,.756.061h.013a3.971,3.971,0,0,0,.757-.073h.024a.314.314,0,0,0,.244-.244l.159-.806a3.4,3.4,0,0,0,.952-.562l.769.281a.3.3,0,0,0,.342-.085l.012-.025a4.1,4.1,0,0,0,.769-1.318v-.024a.282.282,0,0,0,.025-.135.32.32,0,0,0-.11-.207Zm-3.32,1.135a1.684,1.684,0,1,1,1.684-1.685A1.681,1.681,0,0,1,178.907,302.836Z"
              fill="#fff"
            />
          </g>
        </g>
      </g>
    </svg>
  </svg>
);

export default IconCommentOptionsHover;
