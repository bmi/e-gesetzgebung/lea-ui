// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconEyeActive: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (props: SVGProps<SVGSVGElement>) => (
  <svg id="icon-eye-active" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <g id="Gruppe_19341" data-name="Gruppe 19341" transform="translate(-877 -158)">
      <g id="Gruppe_19336" data-name="Gruppe 19336" transform="translate(-577 15549)">
        <rect
          id="Rechteck_6848"
          data-name="Rechteck 6848"
          width="24"
          height="24"
          transform="translate(1454 -15391)"
          fill="none"
        />
      </g>
      <g id="Gruppe_19339" data-name="Gruppe 19339" transform="translate(883.725 165.17)" clip-path="url(#clip-path)">
        <path
          id="Pfad_17637"
          data-name="Pfad 17637"
          d="M11.55,4.91c-.09.16-.17.33-.26.49A8,8,0,0,1,9.61,7.47a5.872,5.872,0,0,1-2.45,1.3,5.547,5.547,0,0,1-1.77.16,5.469,5.469,0,0,1-1.7-.39l1.2-1.2a2.332,2.332,0,0,0,.88.16,2.609,2.609,0,0,0,1.84-.76,2.568,2.568,0,0,0,.76-1.83,2.421,2.421,0,0,0-.16-.89L9.76,2.47l.03.03a7.749,7.749,0,0,1,1.72,2.33l.04.08"
          fill="currentColor"
        />
        <path id="Pfad_17638" data-name="Pfad 17638" d="M7.2,5.03A1.425,1.425,0,0,1,5.89,6.34Z" fill="currentColor" />
        <path
          id="Pfad_17639"
          data-name="Pfad 17639"
          d="M9.65,0,8.23,1.42c-.22-.1-.44-.19-.67-.27A5.53,5.53,0,0,0,5.14.9a5.272,5.272,0,0,0-1.25.28A5.888,5.888,0,0,0,1.94,2.32,7.987,7.987,0,0,0,.16,4.58c-.06.1-.11.21-.16.31v.02c0,.01.01.01.01.02a8.131,8.131,0,0,0,.48.86A7.725,7.725,0,0,0,2.07,7.58L.87,8.78l.88.88L3.13,8.28,4.34,7.07l.84-.84L7.09,4.32l.85-.85L10.21,1.2l.32-.32ZM5.77,3.46A1.435,1.435,0,0,0,4.38,5.27l-.89.89A2.647,2.647,0,0,1,3.17,4.9a2.595,2.595,0,0,1,2.6-2.59,2.638,2.638,0,0,1,1.25.32l-.88.88a1.5,1.5,0,0,0-.37-.05"
          fill="currentColor"
        />
      </g>
    </g>
  </svg>
);

export default IconEyeActive;
