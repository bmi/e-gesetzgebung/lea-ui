// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconFolderDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" {...props}>
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      fill="currentColor"
      d="M19.499 8.83V17a.5.5 0 0 1-.5.5h-14a.508.508 0 0 1-.5-.5V7a.5.5 0 0 1 .5-.5h4.86a.467.467 0 0 1 .34.13l1.85 1.7h6.95a.5.5 0 0 1 .5.5Z"
    />
  </svg>
);

export default SvgIconFolderDefault;
