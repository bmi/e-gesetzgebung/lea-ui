// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconArrowRightBlackHover: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg id="arrow_right_black_hover" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <rect id="Rechteck_1527" data-name="Rechteck 1527" width="24" height="24" transform="translate(0 24) rotate(-90)" />
    <path id="Polygon_2" data-name="Polygon 2" d="M4,0,8,5H0Z" transform="translate(15 8.148) rotate(90)" fill="#fff" />
  </svg>
);

export default SvgIconArrowRightBlackHover;
