// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const IconUndoDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (props: SVGProps<SVGSVGElement>) => (
  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <svg xmlns="http://www.w3.org/2000/svg" width="15.932" height="15.79" x="8" y="8" viewBox="0 0 15.932 15.79">
      <g fill="none" stroke="#161615" strokeLinecap="round" strokeWidth="1.5px" transform="translate(-4.109 -3.858)">
        <path
          strokeMiterlimit="10"
          d="M136.955,75.327h-9.061a5.12,5.12,0,1,0,0,10.227h3.056"
          transform="translate(-117.665 -66.656)"
        />
        <path
          strokeLinejoin="round"
          d="M122.524,81.094l3.751-3.751-3.751-3.751"
          transform="translate(-106.983 -68.674)"
        />
      </g>
    </svg>
  </svg>
);

export default IconUndoDefault;
