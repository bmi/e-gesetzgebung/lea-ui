// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconDirectionRightOutlinedBlack: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
    <g id="Gruppe_14256" data-name="Gruppe 14256" transform="translate(-964 -568)">
      <rect
        id="Rechteck_2281"
        data-name="Rechteck 2281"
        width="24"
        height="24"
        transform="translate(964 568)"
        fill="none"
      />
      <path
        id="Linie_366"
        data-name="Linie 366"
        d="M102.211,152.6a.75.75,0,0,1-.53-1.28l3.471-3.471-3.471-3.471a.75.75,0,0,1,1.061-1.061l4,4a.75.75,0,0,1,0,1.061l-4,4A.748.748,0,0,1,102.211,152.6Z"
        transform="translate(872.289 431.658)"
        fill="#040405"
      />
    </g>
  </svg>
);

export default SvgIconDirectionRightOutlinedBlack;
