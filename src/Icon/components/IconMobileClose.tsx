// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconMobileClose: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (props: SVGProps<SVGSVGElement>) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="1em" height="1em" {...props}>
    <defs>
      <style>{'\r\n      .cls-1{fill:#fff}\r\n    '}</style>
    </defs>
    <path
      fill="currentColor"
      d="m9.41 8 6.3-6.29A1 1 0 1 0 14.29.29L8 6.59 1.71.29A1 1 0 0 0 .29 1.71L6.59 8l-6.3 6.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L8 9.41l6.29 6.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42Z"
    />
  </svg>
);

export default SvgIconMobileClose;
