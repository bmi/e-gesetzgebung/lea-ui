// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconOutlinedTableDefault: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" {...props}>
    <path
      d="M0 0h24v24H0z"
      style={{
        fill: '#fff',
      }}
    />
    <path d="M21.03 4.091h-18a.75.75 0 0 0-.75.75v14a.75.75 0 0 0 .75.75h18a.75.75 0 0 0 .75-.75v-14a.75.75 0 0 0-.75-.75ZM9.713 13.106v-3.24h4.3v3.24Zm4.3 1.5v3.485h-4.3v-3.485ZM3.78 9.866h4.433v3.24H3.78Zm11.737 0h4.763v3.24h-4.763Zm4.763-4.279v2.779H3.78V5.591Zm-16.5 9.015h4.433v3.485H3.78Zm11.737 3.485v-3.481h4.763v3.485Z" />
  </svg>
);

export default SvgIconOutlinedTableDefault;
