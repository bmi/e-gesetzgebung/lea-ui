// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import * as React from 'react';
import { SVGProps } from 'react';

const SvgIconOutlinedTableHover: React.FunctionComponent<React.SVGProps<SVGSVGElement>> = (
  props: SVGProps<SVGSVGElement>,
) => (
  <svg
    id="icon-outlined-table-hover"
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    {...props}
  >
    <g id="Gruppe_5771" data-name="Gruppe 5771" transform="translate(-1006 -112)">
      <rect id="Rechteck_1630" data-name="Rechteck 1630" width="24" height="24" transform="translate(1006 112)" />
    </g>
    <g id="Gruppe_5942" data-name="Gruppe 5942" transform="translate(-842.321 -120.5)">
      <line
        id="Linie_813"
        data-name="Linie 813"
        x2="17.413"
        transform="translate(845.45 129.779)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <line
        id="Linie_814"
        data-name="Linie 814"
        x2="17.413"
        transform="translate(845.45 134.515)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <line
        id="Linie_815"
        data-name="Linie 815"
        y2="9.268"
        transform="translate(857.058 129.779)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <line
        id="Linie_816"
        data-name="Linie 816"
        y2="9.268"
        transform="translate(851.254 129.779)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.5"
      />
      <rect
        id="Rechteck_2718"
        data-name="Rechteck 2718"
        width="18"
        height="14"
        transform="translate(845.321 125.5)"
        fill="none"
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
    </g>
  </svg>
);

export default SvgIconOutlinedTableHover;
