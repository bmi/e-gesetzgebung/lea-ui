// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
/** */
import * as IconComponents from './components';

export type Icons = keyof typeof IconComponents;

export interface IconProps {
  active?: boolean;
  hasHover?: boolean;
}

export interface DefaultIconProps extends IconProps {
  defaultIcon: Icons;
  activeIcon?: Icons;
  hoverIcon?: Icons;
  activeHoverIcon?: Icons;
}

export const Icon: FunctionComponent<DefaultIconProps> = ({
  defaultIcon,
  activeIcon,
  hoverIcon,
  activeHoverIcon,
  hasHover,
  active,
  ...props
}) => {
  let icon: Icons | undefined;
  if (active) {
    icon = hasHover && activeHoverIcon ? activeHoverIcon : activeIcon;
  } else {
    icon = hasHover ? hoverIcon : defaultIcon;
  }
  const Component = IconComponents[icon || defaultIcon];
  return <Component {...props} />;
};

export const ListIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconOutlinedNumberingSymbolsDefault"
      hoverIcon="IconOutlinedNumberingSymbolsHover"
      activeIcon="IconOutlinedNumberingSymbolsActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const ArrowDownBlackIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconArrowDownBlack"
      hoverIcon="IconArrowDownBlackHover"
      activeIcon="IconArrowDownBlackActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const ArrowDownCurrentColorIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconArrowDownCurrentColor" />;
};

export const ArrowRightBlackIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconArrowRightBlack"
      hoverIcon="IconArrowRightBlackHover"
      activeIcon="IconArrowRightBlackActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const ArrowUpBlackIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconArrowUpBlack"
      hoverIcon="IconArrowUpBlackActive"
      activeIcon="IconArrowUpBlackActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const ArrowUpCurrentColorIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconArrowUpCurrentColor" />;
};

export const TableIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconOutlinedTableDefault"
      hoverIcon="IconOutlinedTableHover"
      activeIcon="IconOutlinedTableActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const CreateAuthorialNoteIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconCreateAuthorialNoteDefault"
      hoverIcon="IconCreateAuthorialNoteHover"
      activeIcon="IconCreateAuthorialNoteActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const CommentOptionsIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconCommentOptionsDefault"
      hoverIcon="IconCommentOptionsHover"
      activeIcon="IconCommentOptionsActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const SynopsisOptionsIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconSynopsisOptionsDefault"
      hoverIcon="IconSynopsisOptionsHover"
      activeIcon="IconSynopsisOptionsActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const CreateCommentIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconCreateCommentDefault"
      hoverIcon="IconCreateCommentHover"
      activeIcon="IconCreateCommentActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const EyeIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconEyeDefault"
      hoverIcon="IconEyeHover"
      activeIcon="IconEyeActive"
      activeHoverIcon="IconEyeActiveHover"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const EyeIconCurrentColor: FunctionComponent<IconProps> = ({ active }) => {
  return (
    <Icon
      defaultIcon="IconEyeDefaultCurrentColor"
      activeIcon="IconEyeActiveCurrentColor"
      activeHoverIcon="IconEyeActiveHover"
      active={active}
    />
  );
};

export const SearchReplaceOptionsIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconSearchReplaceOptionsDefault"
      hoverIcon="IconSearchReplaceOptionsHover"
      activeIcon="IconSearchReplaceOptionsActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const DaenvOptionsIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconDaenvOptionsDefault"
      hoverIcon="IconDaenvOptionsHover"
      activeIcon="IconDaenvOptionsActive"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const CompoundDocumentFolderIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconFolderCompundDocumentDefault" />;
};

export const RegulatoryProposalFolderIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconFolderRegulatoryProposalDefault" />;
};

export const OutlinedDirectionalLeftIcon: FunctionComponent<IconProps> = ({ hasHover }) => {
  return (
    <Icon
      defaultIcon="IconOutlinedDirectionalLeftDefault"
      hoverIcon="IconOutlinedDirectionalLeftHover"
      hasHover={hasHover}
    />
  );
};

export const UndoIcon: FunctionComponent<IconProps> = ({ hasHover }) => {
  return <Icon defaultIcon="IconUndoDefault" hoverIcon="IconUndoHover" hasHover={hasHover} />;
};

export const RedoIcon: FunctionComponent<IconProps> = ({ hasHover }) => {
  return <Icon defaultIcon="IconRedoDefault" hoverIcon="IconRedoHover" hasHover={hasHover} />;
};

export const PinIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconPin" />;
};

export const NewestVersionIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconNewsetVersion" />;
};

export const HdrAssistentIcon: FunctionComponent<IconProps> = ({ active, hasHover }) => {
  return (
    <Icon
      defaultIcon="IconHdrAssistenttDefault"
      activeIcon="IconHdrAssistentActive"
      hoverIcon="IconHdrAssistentHover"
      active={active}
      hasHover={hasHover}
    />
  );
};

export const MultipleCommentsIcon: FunctionComponent<IconProps> = () => {
  return <Icon defaultIcon="IconMultipleComments" />;
};
