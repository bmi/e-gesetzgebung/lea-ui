// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import ReactDOM from 'react-dom';

const htmlDivElement: HTMLDivElement | null = document.querySelector('div#app');
ReactDOM.render(<div>Hallo</div>, htmlDivElement);
