// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class EventController {
  public static setMouseEnterHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.addEventListener('mouseenter', callback);
    });
  };

  public static removeMouseEnterHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.removeEventListener('mouseenter', callback);
    });
  };

  public static setMouseLeaveHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.addEventListener('mouseleave', callback);
    });
  };

  public static removeMouseLeaveHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.removeEventListener('mouseleave', callback);
    });
  };

  public static setFocusHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    console.log(element);
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.addEventListener('focus', callback);
    });
  };

  public static removeFocusHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.removeEventListener('focus', callback);
    });
  };

  public static setBlurHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    console.log(element);
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.addEventListener('blur', callback);
    });
  };

  public static removeBlurHandler = (className: string, element: HTMLElement, callback: () => void): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element) => {
      element.removeEventListener('blur', callback);
    });
  };
}
