// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class AriaController {
  public static setElementIdByClassNameInElement = (className: string, idValue: string, element: Element): void => {
    Array.from(element.getElementsByClassName(className)).forEach((element, index) => {
      element.setAttribute('Id', `theme-generic-${idValue}-${index}`);
    });
  };

  public static setElementIdByTagNameInElement = (tagName: string, idValue: string, element: Element): void => {
    Array.from(element.getElementsByTagName(tagName)).forEach((element, index) => {
      element.setAttribute('Id', `theme-generic-${idValue}-${index}`);
    });
  };
}
