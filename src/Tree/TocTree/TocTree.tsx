// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FocusEvent, FunctionComponent, useEffect, useRef, useState } from 'react';
import { Tree as AntdTree } from 'antd';
import { NodeDragEventParams } from 'rc-tree/lib/contextTypes';
import { Key } from 'rc-tree/lib/interface';
import { DataNode, EventDataNode } from 'antd/lib/tree';
/** */
import './TocTree.less';
import { AriaController } from '../../Controller';

export type AllowDropProps = {
  dragNode: DataNode;
  dropNode: DataNode;
  dropPosition: -1 | 0 | 1;
};

interface TocTreeProps {
  showIcon?: boolean;
  ariaLabel: string;
  treeData: DataNode[];
  activeKey: Key | null;
  onActiveChange: (key: Key | null) => void;
  onBlur: (e?: FocusEvent) => void;
  onFocus: (e?: FocusEvent) => void;
  titleRender: (node: DataNode) => React.ReactNode;
  onSelect?: (node: DataNode) => void;
  onRightClick?: (info: any) => void;
  nodeDraggable?: (node: DataNode) => boolean;
  canAddChildren?: (node: DataNode) => boolean;
  onDrop?: (
    info: NodeDragEventParams<DataNode, HTMLDivElement> & {
      dragNode: EventDataNode<DataNode>;
      dragNodesKeys: Key[];
      dropPosition: number;
      dropToGap: boolean;
    },
  ) => void;
  onKeyDown: (event: React.KeyboardEvent<HTMLDivElement>) => void;
  onDragging?: (dragging: boolean) => void;
  allowDrop?: (a: AllowDropProps) => boolean;
  open: boolean;
}

export const TocTree: FunctionComponent<TocTreeProps> = ({
  showIcon,
  treeData,
  ariaLabel,
  activeKey,
  onActiveChange,
  onBlur,
  onFocus,
  titleRender,
  onSelect,
  onRightClick,
  nodeDraggable,
  canAddChildren,
  onDrop,
  onKeyDown,
  onDragging,
  allowDrop,
  open,
}) => {
  const [parsedTreeData, setParsedTreeData] = useState(treeData);
  const [treeHeight, setTreeHeight] = useState(0);
  const tableOfContentRef = useRef<HTMLDivElement>(null);

  const getChildrenOnlyClassname = (dataNode: DataNode) => {
    return canAddChildren && canAddChildren(dataNode) ? 'canAddChildren' : '';
  };

  const parseTreeData = (treeData: DataNode[]): DataNode[] => {
    return treeData.map((treeDataNode) => {
      if (treeDataNode.children && treeDataNode.children.length > 0) {
        return {
          ...treeDataNode,
          children: parseTreeData(treeDataNode.children),
          className: getChildrenOnlyClassname(treeDataNode) + ' ' + treeDataNode.className,
        };
      }
      return { ...treeDataNode, className: treeDataNode.className + ' ' + getChildrenOnlyClassname(treeDataNode) };
    });
  };

  const handleResize = () => {
    setTreeHeight(tableOfContentRef.current?.getBoundingClientRect().height || 0);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  useEffect(() => {
    setParsedTreeData(parseTreeData(treeData));
  }, [treeData]);

  useEffect(() => {
    if (tableOfContentRef.current) {
      AriaController.setElementIdByTagNameInElement('input', 'lea-drawer-tree-input', tableOfContentRef.current);
      setTreeHeight(tableOfContentRef.current.getBoundingClientRect().height);
    }
  }, []);

  return (
    <nav
      className="tableOfContentsNavigation"
      role="treegrid"
      tabIndex={0}
      aria-label={ariaLabel}
      ref={tableOfContentRef}
      onContextMenu={(e) => e.preventDefault()}
    >
      <div className="lea-ui-toc-tree">
        <AntdTree
          onFocus={onFocus}
          showIcon={showIcon}
          checkable={false}
          selectable={false}
          onClick={(_e, node) => onSelect && onSelect(node)}
          defaultExpandAll={true}
          treeData={parsedTreeData}
          virtual={false}
          disabled={!open}
          height={treeHeight}
          onActiveChange={onActiveChange}
          activeKey={activeKey}
          onBlur={onBlur}
          allowDrop={allowDrop}
          titleRender={titleRender}
          onDragStart={() => {
            onDragging && onDragging(true);
          }}
          onDragEnd={() => {
            onDragging && onDragging(false);
          }}
          draggable={
            nodeDraggable
              ? {
                  icon: false,
                  nodeDraggable,
                }
              : false
          }
          onDrop={onDrop}
          onKeyDown={onKeyDown}
          onRightClick={onRightClick}
        />
      </div>
    </nav>
  );
};
