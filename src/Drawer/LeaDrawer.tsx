// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Drawer } from 'antd';
import React, { FunctionComponent, MouseEvent, useCallback, useState } from 'react';
import { CloseOutlined } from '../Icon/components/CloseOutlined';

/** */
import './Drawer.less';

interface LeaDrawerProps {
  children: any;
  title: string;
  placement: 'left' | 'right' | 'bottom';
  open: boolean;
  onClose?: () => void;
  width?: number;
  expandable?: boolean;
  refProp?: React.RefObject<HTMLDivElement>;
  ariaLabel?: string;
  onFocus?: () => void;
  className?: string;
}

declare type getContainerFunc = () => HTMLElement;

const defaultDrawerWidth = 368;
const minDrawerWidth = 184;
const maxDrawerWidth = 1104;

export const LeaDrawer: FunctionComponent<LeaDrawerProps> = ({
  children,
  title,
  placement,
  open,
  onClose,
  width,
  expandable,
  refProp,
  ariaLabel,
  onFocus,
  className,
}) => {
  const [selected, setSelected] = useState(false);
  const [drawerWidth, setDrawerWidth] = useState(width ?? defaultDrawerWidth);

  const handleMouseDown = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    document.addEventListener('mouseup', handleMouseUp, true);
    document.addEventListener('mousemove', handleMouseMove, true);
  };

  const handleMouseUp = (e: globalThis.MouseEvent) => {
    document.removeEventListener('mouseup', handleMouseUp, true);
    document.removeEventListener('mousemove', handleMouseMove, true);
  };

  const handleMouseMove = useCallback((e: globalThis.MouseEvent) => {
    if (refProp?.current) {
      const newWidth =
        placement === 'right'
          ? refProp.current.getBoundingClientRect().right - e.clientX
          : e.clientX - refProp.current.getBoundingClientRect().x;
      if (newWidth > minDrawerWidth && newWidth < maxDrawerWidth) {
        setDrawerWidth(newWidth);
      }
    }
  }, []);

  const handleOpenFocus = useCallback((open: boolean) => {
    if (!open || !refProp?.current) return;
    const drawer = refProp.current.getElementsByClassName('ant-drawer')[0];
    if (drawer) {
      for (const drawerChildren of drawer.children) {
        const dataSentinel = drawerChildren.getAttribute('data-sentinel');
        if (dataSentinel === 'start' || dataSentinel === 'end') {
          drawerChildren.setAttribute('tabIndex', '-1');
        }
      }
    }
    const antDrawerFocusElement = refProp.current.getElementsByClassName('ant-drawer-inline')[0] as HTMLElement;
    setTimeout(() => antDrawerFocusElement.focus());
  }, []);

  return (
    <aside
      className={`lea-drawer${className ? ' ' + className : ''}${selected ? ' lea-drawer-selected' : ''}`}
      ref={refProp}
      onFocus={() => {
        setSelected(true);
        onFocus?.();
      }}
      onBlur={() => setSelected(false)}
      aria-label={ariaLabel}
      aria-hidden={!open}
      tabIndex={open ? 0 : -1}
    >
      <Drawer
        title={title}
        placement={placement}
        onClose={() => onClose && onClose()}
        closeIcon={<CloseOutlined />}
        closable={open}
        getContainer={false}
        rootStyle={{ position: 'absolute' }}
        open={open}
        mask={false}
        width={drawerWidth}
        afterOpenChange={handleOpenFocus}
      >
        {expandable && (
          <div
            onMouseDown={(e: MouseEvent<HTMLElement>) => handleMouseDown(e)}
            className={`drawer-dragger drawer-dragger-${placement}`}
          />
        )}
        {children}
      </Drawer>
    </aside>
  );
};
