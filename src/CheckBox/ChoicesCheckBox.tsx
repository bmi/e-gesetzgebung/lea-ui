// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './ChoicesCheckBox.less';

import { Checkbox, Divider } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

interface ChoicesCheckBoxInterface {
  choicesList: ChoicesItemInterface[];
  isVisible: boolean;
  allCheckable?: boolean;
  checkedList: CheckboxValueType[];
  setCheckedList: (valueList: CheckboxValueType[]) => void;
}
export interface ChoicesItemInterface {
  label: string;
  value: CheckboxValueType;
  disabled?: boolean;
  onChange?: (e: CheckboxChangeEvent) => void;
}
export function ChoicesCheckBox({
  choicesList,
  isVisible,
  allCheckable,
  checkedList,
  setCheckedList,
}: ChoicesCheckBoxInterface): React.JSX.Element {
  const { t } = useTranslation();
  const CheckboxGroup = Checkbox.Group;
  const valuesList = choicesList.map((item) => item.value);
  const [indeterminate, setIndeterminate] = useState(false);
  const [checkAll, setCheckAll] = useState(false);

  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    setCheckedList(e.target.checked ? valuesList : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  const onChange = (list: CheckboxValueType[]) => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < choicesList.length);
    setCheckAll(list.length === choicesList.length);
  };

  useEffect(() => {
    setIndeterminate(checkedList.length < choicesList.length);
    setCheckAll(checkedList.length === choicesList.length);
  }, []);

  return (
    <div className={`choices-check-box ${!isVisible ? 'hidden' : ''}`}>
      {allCheckable && (
        <>
          <Checkbox indeterminate={indeterminate} onChange={onCheckAllChange} checked={checkAll} className="check-all">
            {t('lea.commentOptions.filter.checkAll')}
          </Checkbox>
          <Divider className="check-all-divider" />
        </>
      )}
      <fieldset className="choices-list">
        <CheckboxGroup options={choicesList} value={checkedList} onChange={onChange} role="list" />
      </fieldset>
    </div>
  );
}
