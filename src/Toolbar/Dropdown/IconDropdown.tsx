// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent, useEffect } from 'react';
import { Dropdown } from 'antd';

import { ArrowDownBlackIcon } from '../../Icon';
import { IconButton } from '../../Button';

import './ToolbarDropdown.less';
import { ItemType } from 'antd/lib/menu/hooks/useItems';
import { focusFirstItem } from './dropdownHelper';
import { IconDirectionRightOutlinedBlack } from '../../Icon/components';

interface IconDropdownProps {
  onActive: (active: boolean) => void;
  isActive: boolean;
  icon: React.JSX.Element;
  id: string;
  label?: string;
  ariaLabel?: string;
  disabled?: boolean;
  items: ItemType[];
  onHover?: (hovered: boolean) => void;
  onClick?: () => void;
  keyDownHandler?: (e: React.KeyboardEvent<HTMLElement>) => void;
  selectable?: boolean;
  hasHover?: boolean;
  buttonRef: React.RefObject<HTMLButtonElement>;
}

export const IconDropdown: FunctionComponent<IconDropdownProps> = ({
  id,
  items,
  buttonRef,
  disabled,
  isActive,
  onActive,
  onClick,
  keyDownHandler,

  onHover,
  label,
  ariaLabel,
  icon,
  selectable,
  hasHover,
}) => {
  const handleButtonKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    if (event.altKey && event.key.toLowerCase() === 'arrowdown') {
      onActive(true);
    }
  };

  const visibleChangeHandler = (visible: boolean) => {
    onActive(visible);
  };

  useEffect(() => {
    if (isActive) {
      focusFirstItem(items, id);
    }
  }, [isActive, items]);

  useEffect(() => {
    buttonRef.current?.addEventListener('focus', () => onHover && onHover(true));
    buttonRef.current?.addEventListener('blur', () => {
      onHover && onHover(false);
    });
  }, []);

  const handleFocusChange = (e: React.FocusEvent) => {
    if (e.target) {
      const checkbox = e.target.getElementsByClassName('ant-checkbox-input')[0] as HTMLElement;
      if (checkbox) {
        e.preventDefault();
        setTimeout(() => checkbox.focus());
      }
    }
  };

  return (
    <div
      className="lea-ui-icon-dropdown"
      onMouseEnter={() => onHover && onHover(true)}
      onMouseLeave={() => onHover && onHover(false)}
      onFocus={() => onHover && onHover(true)}
      onBlur={() => onHover && onHover(false)}
    >
      <Dropdown
        className="lea-ui-toolbar-dropdown"
        open={isActive}
        menu={{
          id,
          expandIcon: <IconDirectionRightOutlinedBlack />,
          items,
          selectable,
          onKeyDown: keyDownHandler,
          onClick,
          onFocus: handleFocusChange,
        }}
        disabled={disabled}
        onOpenChange={visibleChangeHandler}
        trigger={['click']}
        overlayClassName="lea-ui-toolbar-dropdown-children"
      >
        <IconButton
          id={`${id}-button`}
          buttonRef={buttonRef}
          title={label}
          icon={icon}
          disabled={disabled}
          aria-haspopup
          aria-expanded={isActive}
          aria-label={ariaLabel}
          onKeyDown={handleButtonKeyDown}
        >
          <ArrowDownBlackIcon active={isActive} hasHover={hasHover} />
        </IconButton>
      </Dropdown>
    </div>
  );
};
