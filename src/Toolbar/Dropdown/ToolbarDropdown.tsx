// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Dropdown } from 'antd';
import React, { FunctionComponent, useEffect, useState } from 'react';
import TextButton from '../../Button/TextButton';
import { ArrowDownBlackIcon } from '../../Icon';

import './ToolbarDropdown.less';
import { ItemType } from 'antd/lib/menu/hooks/useItems';
import { focusFirstItem } from './dropdownHelper';
import { IconDirectionRightOutlinedBlack } from '../../Icon/components';

interface ToolbarDropdownProps {
  id: string;
  onActive: (active: boolean) => void;
  isActive: boolean;
  header: string;
  disabled?: boolean;
  arrowIconVisible?: boolean;
  buttonRef: React.RefObject<HTMLButtonElement>;
  items: ItemType[];
  openKeys?: string[];
  keyDownHandler?: (e: React.KeyboardEvent<HTMLElement>) => void;
  setOpenKeys?: (value: React.SetStateAction<string[]>) => void;
  onClick?: () => void;
  selectable?: boolean;
}
export const ToolbarDropdown: FunctionComponent<ToolbarDropdownProps> = ({
  id,
  items,
  buttonRef,
  disabled,
  isActive,
  onActive,
  onClick,
  keyDownHandler,

  setOpenKeys,
  selectable,
  header,
  arrowIconVisible,
  openKeys,
}) => {
  const [isHover, onHover] = useState(false);

  const handleButtonKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
    if (event.altKey && event.key.toLowerCase() === 'arrowdown') {
      onActive(true);
    }
  };

  const visibleChangeHandler = (visible: boolean) => {
    setTimeout(() => onActive(visible));
  };

  useEffect(() => {
    if (isActive) {
      focusFirstItem(items, id);
    }
  }, [isActive, items.length]);

  useEffect(() => {
    buttonRef.current?.addEventListener('focus', () => onHover(true));
    buttonRef.current?.addEventListener('blur', () => {
      onHover(false);
    });
  }, []);

  const dropdownClassName = (isActive: boolean, isHover: boolean) => {
    if (isActive) {
      return ' active';
    } else if (isHover) {
      return ' hover';
    }
    return '';
  };

  const handleOpenChange = (openKeys: string[]) => {
    setOpenKeys && setOpenKeys(openKeys);
  };

  const handleFocusChange = (e: React.FocusEvent) => {
    if (e.target) {
      const checkbox = e.target.getElementsByClassName('ant-checkbox-input')[0] as HTMLElement;
      if (checkbox) {
        e.preventDefault();
        setTimeout(() => checkbox.focus());
      }
    }
  };

  return (
    <Dropdown
      open={isActive}
      className={`lea-ui-toolbar-dropdown${dropdownClassName(isActive, isHover)}`}
      destroyPopupOnHide={false}
      menu={{
        id,
        expandIcon: <IconDirectionRightOutlinedBlack />,
        items,
        selectable,
        openKeys,
        onOpenChange: handleOpenChange,
        onKeyDown: keyDownHandler,
        onClick,
        onFocus: handleFocusChange,
      }}
      disabled={disabled}
      onOpenChange={visibleChangeHandler}
      trigger={['click']}
      overlayClassName="lea-ui-toolbar-dropdown-children"
    >
      <TextButton
        buttonRef={buttonRef}
        onKeyDown={handleButtonKeyDown}
        onHover={onHover}
        id={`${id}-button-${header.replace(' ', '-').toLowerCase()}`}
      >
        {header}
        {arrowIconVisible && <ArrowDownBlackIcon active={isActive} hasHover={isHover} />}
      </TextButton>
    </Dropdown>
  );
};
