// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ItemType, MenuItemType, SubMenuType } from 'antd/lib/menu/hooks/useItems';

export const focusFirstItem = (items: ItemType[], id: string) => {
  if (items.length === 0) return;
  const item = items.find((item) => {
    return item && !(item as MenuItemType | SubMenuType).disabled;
  });
  if (item) {
    const element = document.querySelector(`[data-menu-id="${id}-${item.key || ''}"]`) as HTMLElement;
    if (element) {
      setTimeout(() => element.focus());
    }
  }
};
