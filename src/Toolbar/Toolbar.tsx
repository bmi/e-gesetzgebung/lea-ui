// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
//
import './Toolbar.less';

type Props = {
  children?: React.ReactNode;
  className?: string;
  ariaLabel?: string;
};
export const Toolbar: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={'lea-ui-toolbar'} {...props} />
);
export const ToolbarLeft: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={'toolbar-left'} {...props} />
);
export const ToolbarMiddle: FunctionComponent<Props> = ({ className, ariaLabel, ...props }) => (
  <nav tabIndex={0} className={'toolbar-middle'} aria-label={ariaLabel} {...props} />
);
export const ToolbarRight: FunctionComponent<Props> = ({ className, ...props }) => (
  <div className={'toolbar-right'} {...props} />
);

export default Object.assign(Toolbar, {
  Left: ToolbarLeft,
  Middle: ToolbarMiddle,
  Right: ToolbarRight,
});
